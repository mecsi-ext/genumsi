<?php
function sanitize($s){
    require_once 'htmlpurifier-4.12.0-lite/library/HTMLPurifier.auto.php';
    
    $purifier = new HTMLPurifier();

    return htmlentities(trim($purifier->purify($s)), ENT_QUOTES);
}